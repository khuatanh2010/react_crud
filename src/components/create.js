import React, { useState } from 'react';
import { Button,  Form } from 'semantic-ui-react'
import axios from 'axios';

export default function Create() {
    const [username, sestUsername] = useState('');
    const [password, setPassWord] = useState('');
    
    const postData = () => {
        axios.post('http://localhost:8080/api/user', {
            username,
            password
        })
    }
    return (
        <div>
            <Form className="create-form">
                <Form.Field>
                    <label>Username</label>
                    <input placeholder='Username' onChange={(e) => sestUsername(e.target.value)}/>
                </Form.Field>
                <Form.Field>
                    <label>Password</label>
                    <input placeholder='Password' onChange={(e) => setPassWord(e.target.value)}/>  
                </Form.Field>
                <Button onClick={postData} type='submit'>Submit</Button>
            </Form>
        </div>
    )
}