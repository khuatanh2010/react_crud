import React, { useState, useEffect } from 'react';
import { Table } from 'semantic-ui-react'
import axios from 'axios';



export default function Read() {
    const [APIData, setAPIData] = useState([]);
    useEffect(() => {
        axios.get('http://localhost:8080/api/user')
            .then((response) => {
                setAPIData(response.data);
            })
    }, [])
    return (
        <div>
            <Table singleLine>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Username</Table.HeaderCell>
                        <Table.HeaderCell>Password</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                {APIData.map((data) => {
                    return (
                        <Table.Row>
                            <Table.Cell>{data.username}</Table.Cell>
                            <Table.Cell>{data.password}</Table.Cell>
                        </Table.Row>
                    )
                })}
            </Table>
        </div>
    )
}