import React, { useState, useEffect } from 'react';
import { Table, Button } from 'semantic-ui-react'
import axios from 'axios';
import { Link } from 'react-router-dom';


export default function Update() {
    const [APIData, setAPIData] = useState([]);
    useEffect(() => {
        axios.get('http://localhost:8080/api/user')
            .then((response) => {
                setAPIData(response.data);
            })
    }, [])
    const setData = (data) => {
        console.log(data);
}
     
    return (
        <div>
            <Table singleLine>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>Username</Table.HeaderCell>
                        <Table.HeaderCell>Password</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                {APIData.map((data) => {
                    return (
                        
                        <Table.Row>
                            <Table.Cell>{data.id}</Table.Cell>
                            <Table.Cell>{data.username}</Table.Cell>
                            <Table.Cell>{data.password}</Table.Cell>
                            <Link to='/update'>
                                <Table.Cell>
                                <Button onClick={() => setData()}>Update</Button>
                                </Table.Cell>
                            </Link>
                        </Table.Row>
                    )
                })}
            </Table>
        </div>
    )
}